const scroll = () => {
    window.onscroll = () => {stickyFun()};
    const header =  document.querySelector('.js-header');
    const sticky =  header.offsetTop;
    function stickyFun() {
        if(window.pageYOffset > sticky) {
            header.classList.add('fixed');
        } else {
            header.classList.remove('fixed');
        }
    }
}
scroll();

const scrollTo = () => {
    const anchors = document.querySelectorAll('a.scroll-to')
    for (let anchor of anchors) {
        anchor.addEventListener('click', function (e) {
            e.preventDefault()      
            const blockID = anchor.getAttribute('href')       
            document.querySelector(blockID).scrollIntoView({
               behavior: 'smooth',
               block: 'start'
            })
        })
    }
}
scrollTo();

const openMenu = () => {
    const iconMenu = document.querySelector('.js-burger');
    if(iconMenu) {
        const menuBody = document.querySelector('.js-burger-menu');
        iconMenu.addEventListener('click', () => {
            iconMenu.classList.toggle('active');
            menuBody.classList.toggle('active');
        });
    }
}
openMenu();

const timer = () => {
    let data = new Date(2021, 10, 1, 0, 0, 0,);

    const count = () => {
        let nowData = new Date();
        let gap = data - nowData;
        
        let days = Math.floor(gap / 1000 / 60 / 60 / 24);
        let hours = Math.floor(gap / 1000 / 60 / 60) % 24;
        let minutes = Math.floor(gap / 1000 / 60) % 60;
        let seconds = Math.floor(gap / 1000) % 60;

        if(gap < 0) {
            document.querySelector(".timer").classList.add('active')
        } else{
        document.querySelector(".days").innerText = days;
        document.querySelector(".hours").innerText = hours;
        document.querySelector(".minutes").innerText = minutes;
        document.querySelector(".seconds").innerText = seconds;
       } 
    }
    count()

    setInterval(count, 1000);   
}
timer();


